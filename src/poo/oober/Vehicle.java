package poo.oober;

/**
 * Created by theofrasquet on 29/09/16.
 */
public class Vehicle {
    // shuttle's current location
    private Location location;  // package access for testing
    // where the shuttle is headed
    private Location targetLocation;  // package access for testing

    protected Vehicle(Location location) {
        this.location = location;
    }

    protected Location getLocation() {
        return location;
    }

    protected Location getTargetLocation() {
        return targetLocation;

    }

    public boolean setPickupLocation(Location location) {
        if (isFree()) {
            setTargetLocation(location);
            return true;
        }
        return false;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setTargetLocation(Location targetLocation) {
        this.targetLocation = targetLocation;
    }

    public boolean isFree() {
        return getTargetLocation() == null;
    }

    public boolean offload() {
        setLocation(getTargetLocation());
        setTargetLocation(null);
        return getTargetLocation() == null && isFree();
    }

    public boolean pickup(Location destination) {
        setLocation(getTargetLocation());
        setTargetLocation(destination);
        return getTargetLocation() != null;
    }

}
