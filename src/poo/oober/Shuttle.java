package poo.oober;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;


/**
 * A shuttle is able to carry multiple passengers.
 * The shuttle is initially idle and empty of passengers - it sits at a
 * location waiting for a pickup location.
 * It receives a pickup location which gets added to the list of
 * destinations and a target location is chosen from the list.
 * The shuttle picks up a passenger at the target location
 * and a new target location is chosen from the list.
 * The shuttle offloads a passenger at the target location.
 * A new target location is chosen from the list of destinations.
 * Much depends on the definition of isFree. For the moment we'll use about the
 * simplest implementation - the shuttle is always free to accept
 * more destinations.
 * For the moment, choosing the next target location is about as simple as
 * possible - a random choice from the destination list.
 * For the moment, again, we ignore how the shuttle actually moves from one
 * location to another.
 *
 * @author David J. Barnes and Michael Kölling
 * @author <code><a href="mailto:sander@unice.fr">Peter Sander</a></code>
 */
public class Shuttle extends Vehicle{
    // list of destinations for the shuttle
    //  package access for testing
    List<Location> destinations = new ArrayList<>();
    
    /**
     * Default constructor for objects of class Shuttle.
     * Assigns the default maxCapacity as maxCapacity.
     * @param location The shuttle's starting point.
     */
    public Shuttle(Location location) {
        super(location);
    }

    /**
     * Currently a shuttle is always free.
     * We can't assume this will always be the case so we insist on using
     * this method.
     * @return Whether or not this shuttle is free.
     */
    @Override
    public boolean isFree() {
        return true;
    }
    
    /**
     * Receive a pickup location.
     * If the shuttle is free, the location is added to the list of
     * destinations and a new target location is chosen from the list.
     * @param pickupLocation The pickup location.
     * @return true if the pickup location has been added to the destinations,
     * false otherwise.
     */
    @Override
    public boolean setPickupLocation(Location pickupLocation) {
        if (isFree()) {
            destinations.add(pickupLocation);
            setTargetLocation(chooseTargetLocation());
            return true;
        }
        return false;
    }

    /**
     * Determines whether a location is one of the shuttle destinations.
     * @param location Potential destination, or not.
     * @return true if the location is in fact a destination, false if not.
     */
    boolean isaDestination(Location location) {
        return destinations.contains(location);
    }
    
    /**
     * Pickup a passengerat the current target location.
     * Remove the current location from the destinations (because the shuttle
     * is there already), and add the destination to the list.
     * A new target location is chosen.
     * @param destination The passenger's destination.
     * @return true if tha passenger was picked up; false otherwise.
     */
    @Override
    public boolean pickup(Location destination) {
        if (isFree()) {
            setLocation(getTargetLocation());
            destinations.remove(getLocation());
            destinations.add(destination);
            setTargetLocation(chooseTargetLocation());
            return true;
        }
        return false;
    }

    /**
     * Offload the passenger whose destination is the current target location.
     * A new  target location is chosen.
     * @return true if tha passenger was offloaded; false otherwise.
     */
    @Override
    public boolean offload() {
        setLocation(getTargetLocation());
        setTargetLocation(chooseTargetLocation());
        return true;  // got a better idea?
    }

    /**
     * Decides new target location, based on the list of possible destinations.
     * Removes new targetLocation from the list of destinations.
     * Currently we don't really know how to do this correctly so just
     * takes a random destination.
     * @return New target location;
     */
    Location chooseTargetLocation() {  // package access for testing
        if (destinations.size() > 0) {
            int index = new Random().nextInt(destinations.size());
            return destinations.remove(index);
        }
        return null;
    }
}
