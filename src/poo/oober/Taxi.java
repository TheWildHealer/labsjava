package poo.oober;


/**
 * A taxi is able to carry a single passenger.
 * The taxi is initially idle - it sits at a location waiting for a
 * pickup location.
 * It receives a pickup location which becomes its target location
 * to go pick up a passenger.
 * The taxi picks up a passenger at the target location, and gets a new
 * target where to go deliver the passenget.
 * The taxi offloads the passenger at the target location.
 * The target location becomes null until the taxi receives the
 * next pickup request.
 * For the moment, we ignore how the taxi actually moves from one
 * location to another.
 * 
 * @author David J. Barnes and Michael Kölling
 * @author <code><a href="mailto:sander@unice.fr">Peter Sander</a></code>
 */
public class Taxi extends Vehicle {

    /**
     * Constructor for objects of class Taxi.
     * @param location The taxi's starting point.
     */
    public Taxi(Location location) {
        super(location);
    }

}
