package poo.oober;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;


/**
 * Model the operation of a non-taxi company, operating different
 * types of vehicle (taxis and shuttles).
 * The difficulty is in maintaining separate functionality for the different
 * types of vehicles. For example, when a passenger requests a vehicle, they
 * indicate a type of vehicle, and the company must dispatch that type
 * of vehicle.
 * 
 * @author David J. Barnes and Michael Kölling
 * @author <code><a href="mailto:sander@unice.fr">Peter Sander</a></code>
 */
public class Company {
    List<Vehicle> vehicles;  // package access for testing
    Vehicle vehicle;

    /**
     * Constructor for objects of class Company.
     * Note that this constructor has array arguments, but that
     * taxis and shuttles are stored as lists.
     * @param vehicles Company's vehicles.
     */
    public Company(Vehicle[] vehicles) {
        this(new ArrayList<Vehicle>(Arrays.asList(vehicles)));
    }

    /**
     * Constructor for objects of class Company.
     * @param vehicles Company's vehicles.
     */
    public Company(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    /**
     * Request a pickup for a given passenger and a given type of vehicle,
     * either a taxi or a shuttle.
     * Chooses an available taxi via the scheduleTaxi method, or an
     * available shuttle via the scheduleShuttle method.
     * Routes the request to the available vehicle by calling its
     * setPickupLocation method.
     * Assigns passenger to their vehicle in either the taxiAssignments
     * or the shuttleAssignments map.
     * You may want to look at the documentation for java.lang.Class.
     * @return Whether a free vehicle of the requested class is available.
     */
    public boolean requestPickup(Location pickupLocation) {
        vehicle = scheduleVehicle();
        if (vehicle != null) {
            vehicle.setPickupLocation(pickupLocation);
            return true;
        }
        return false;
    }
    
    /**
     * A vehicle has arrived at a pickup point.
     * @param vehicle The vehicle at the pickup point.
     * @param destination Passenger destination.
     * @return true if passenger picked up; false otherwise.
     */
    public boolean arrivedAtPickup(Vehicle vehicle, Location destination) {
        return vehicle.pickup(destination);
    }
    
    /**
     * A vehicle has arrived at a passenger's destination.
     * Offloads passenger.
     * @param vehicle The vehicle at the destination.
     * @return true if passenger picked up; false otherwise.
     */
    public boolean arrivedAtDestination(Vehicle vehicle) {
        return vehicle.offload();
    }

    /**
     * Find a free vehicle, if any.
     * Currently just selects first free vehicle.
     * @return A free Vehicle, or null if there is none.
     */
    private Vehicle scheduleVehicle() {
        return vehicles.stream()
                .filter(s -> s.isFree())
                .findFirst()
                .orElse(null);
    }

}
