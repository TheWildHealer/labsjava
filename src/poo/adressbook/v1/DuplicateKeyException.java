package poo.adressbook.v1;

/**
 * Thrown if a key is already in use.
 * @author Peter Sander
 */
@SuppressWarnings("serial")
public class DuplicateKeyException extends RuntimeException {

    /**
     * Constructor. Stores message with superclass.
     * @param message
     */
    public DuplicateKeyException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "Key already in use: " + getMessage() + ".";
    }
}

