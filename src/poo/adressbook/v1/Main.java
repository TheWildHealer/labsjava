package poo.adressbook.v1;

/**
 * Created by theofrasquet on 10/10/16.
 */
public class Main {

    public static void main(String[] args) {
        AddressBookTextInterface a = new AddressBookTextInterface(new AddressBook());
        a.run();
    }

}
