package poo.lab1.labclasses;

/**
 * Created by Theo on 07/09/2016.
 */
public class Main {

    public static void main(String[] args) {
        LabClass labClass = new LabClass(100);
        System.out.println(labClass.numberOfStudents());
        Student student1 = new Student("Joe Dalton","123");
        student1.addCredits(6);
        Student student2 = new Student("Timothy Dalton","456");
        student2.addCredits(4);
        labClass.enrollStudent(student1);
        labClass.enrollStudent(student2);
        labClass.setRoom("O+110");
        labClass.setInstructor("Peter Sander");
        labClass.setTime("07/09/2016 at 10:15");
        System.out.println(labClass.numberOfStudents());
        labClass.printList();
    }

}
