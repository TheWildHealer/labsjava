package poo.lab1.figures;
/**
 * @author (c)2011 <a href="mailto:sander@polytech.unice.fr">Peter Sander</a>
 *
 */
public class Main {
    /**
     * @param args Unused.
     */
    public static void main(String[] args) {
        Square square = new Square();
        square.changeColor("red");
        square.changeSize(150);
        square.makeVisible();
        square.moveHorizontal(-150);
        square.moveVertical(20);
        Square square2 = new Square();
        square2.changeColor("black");
        square2.changeSize(50);
        square2.makeVisible();
        square2.moveHorizontal(-120);
        square2.moveVertical(50);
        Triangle triangle = new Triangle();
        triangle.changeColor("green");
        triangle.changeSize(75,250);
        triangle.makeVisible();
        triangle.moveHorizontal(25);
        triangle.moveVertical(-75);
        Circle circle =new Circle();
        circle.changeColor("yellow");
        circle.makeVisible();
        circle.moveHorizontal(100);
        circle.moveVertical(-75);
    }
}