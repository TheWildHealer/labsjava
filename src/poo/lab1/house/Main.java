package poo.lab1.house;

/**
 * Created by Theo on 07/09/2016.
 */
public class Main {

    public static void main(String[] args) {
        Picture picture = new Picture();
        picture.draw();
        picture.sunset();
    }

}
