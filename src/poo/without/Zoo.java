package poo.without;

import java.util.List;

/**
 * Zoo containing animals.
 * @author Peter Sander
 */
public class Zoo {

    private List animals;

    /**
     * Constructor.
     * @param animals Animals housed in the zoo.
     */
    public Zoo(List animals) {
        this.animals = animals;
    }

    /**
     * @return Information about all the animals in the zoo.
     */
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i  = 0; i< animals.size(); i++) {
            stringBuilder.append(((Animal)animals.get(i)).getNames());
        }
        return stringBuilder.toString();
    }

    /**
     * @param animal Makes this species of animal talk.
     * @return Favourite saying.
     */
    public String makeItTalk(Animal animal) {
        return animal.talk();
    }
}
