package poo.without;


/**
 * Represents talking animals.
 * @author ta race
 */
public interface Animal {
    String getNames();
    String talk();
}
