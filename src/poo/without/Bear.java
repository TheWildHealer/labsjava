package poo.without;

/**
 * Created by theofrasquet on 06/10/16.
 */
public class Bear implements Animal{
    private String name;
    private String talk;

    public Bear(String name, String talk) {
        this.name = name;
        this.talk = talk;
    }

    public Bear() {
        this("Bear","Bear says: Smarter than the average beer.");
    }

    @Override
    public String getNames() {
        return name;
    }

    @Override
    public String talk() {
        return talk;
    }
}
