package poo.without;

/**
 * Created by theofrasquet on 06/10/16.
 */
public class Roadrunner implements Animal{
    private String name;
    private String talk;

    public Roadrunner(String name, String talk) {
        this.name = name;
        this.talk = talk;
    }

    public Roadrunner() {
        this("Roadrunner","Roadrunner says: Hmeep hmeep!");
    }

    @Override
    public String getNames() {
        return name;
    }

    @Override
    public String talk() {
        return talk;
    }
}
