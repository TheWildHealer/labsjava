package poo.without;

/**
 * Created by theofrasquet on 06/10/16.
 */
public class Walrus implements Animal{
    private String name;
    private String talk;

    public Walrus(String name, String talk) {
        this.name = name;
        this.talk = talk;
    }

    public Walrus() {
        this("Walrus","Walrus says: I am the walrus.");
    }

    @Override
    public String getNames() {
        return name;
    }

    @Override
    public String talk() {
        return talk;
    }
}
