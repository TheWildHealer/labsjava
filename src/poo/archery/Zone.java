package poo.archery;

import static poo.archery.Target.NUM_ZONE;
import static poo.archery.Target.ZONE_COLOURS;
import static poo.archery.Target.RADIUS;

/**
 * Created by Theo on 19/09/2016.
 */
public class Zone {
    private int points;
    private int number;
    private String color;
    private double radius;
    private double area;
    private double percentTotalArea;

    public Zone(int points){
        this.points = points;
        this.number = NUM_ZONE[10-points];
        this.color = ZONE_COLOURS[10-points];
        this.radius = RADIUS[10-points];
    }

    public int getPoints(){
        return 0;
    }

    public int getNumber(){
        return 0;
    }

    public String getColour(){
        return null;
    }

    public double getRadius(){
        return 0;
    }

    public double area(){
        return 0;
    }

    public double percentTotalArea(){
        return 0;
    }

}
