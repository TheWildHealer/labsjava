package poo.archery;

/**
 * Created by Theo on 19/09/2016.
 */
public class Target {
    public final static int[] NUM_ZONE = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    public final static double[] RADIUS = {6.1, 12.2, 18.3, 24.4, 30.5, 36.6, 42.7, 48.8, 54.9, 61.0};
    public final static String[] ZONE_COLOURS = {"gold","gold","red","red","blue","blue","black","black","white","white"};
}
