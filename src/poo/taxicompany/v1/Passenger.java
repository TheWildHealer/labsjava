package poo.taxicompany.v1;

/**
 * Created by Theo on 09/09/2016.
 */
public class Passenger {
    private Location destination;

    public Passenger(String name, Location location, Location destination) {
        this.destination = destination;
    }

    public Location getDestination() {
        return destination;
    }

}
