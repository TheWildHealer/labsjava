package poo.taxicompany.v5;

/**
 * Simulate passengers requesting rides from a taxi company.
 * Passengers should be generated at random intervals.
 *
 * @author David J. Barnes and Michael Kölling
 * @version 2016.02.29
 */
public class PassengerSource {
    private static int number = 0;
    private TaxiCompany company;

    /**
     * Constructor for objects of class PassengerSource.
     * @param company The company to be used. Must not be null.
     * @throws NullPointerException if company is null.
     */
    public PassengerSource(TaxiCompany company) {
        if(company == null) throw new NullPointerException();
        this.company = company;
    }

    /**
     * Have the source generate a new passenger and
     * request a pickup from the company.
     * @return true If the request succeeds, false otherwise.
     */
    public boolean requestPickup() {
        return this.company.requestPickup(createPassenger());
    }

    /**
     * Create a new passenger.
     * The first passenger created is named "Passenger1",
     * the second is "Passenger2" and so on for each new
     * passenger.
     * @return The created passenger.
     */
    private Passenger createPassenger() {
        number++;
        return new Passenger("Passenger"+number,new Location(), new Location());
    }
}