package poo.taxicompany.v5;

public class Passenger {
    private String name;
    private Location pickup;
    private Location destination;

    /**
     * Constructor for objects of class Passenger
     *
     * @param name        Name of passenger.
     * @param pickup      The pickup location.
     * @param destination The destination location.
     */
    public Passenger(String name, Location pickup, Location destination) {
        this.name = name;
        this.pickup = pickup;
        this.destination = destination;
    }

    /**
     * @return The pickup location.
     */
    public Location getPickupLocation() {
        return pickup;
    }

    /**
     * @return The destination location.
     */
    public Location getDestination() {
        return destination;
    }
}