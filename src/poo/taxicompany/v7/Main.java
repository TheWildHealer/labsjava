package poo.taxicompany.v7;

/**
 * Created by Theo on 22/09/2016.
 */
public class Main {

    public static void main(String[] args) {
        /*
        Taxi[] taxis = new Taxi[2];
        Arrays.setAll(taxis, (e) -> {return new Taxi(null);});
        Shuttle[] shuttles = new Shuttle[3];
        Arrays.setAll(shuttles, (e) -> {return new Shuttle(null);});
        TaxiCompany taxico = new TaxiCompany(Arrays.asList(taxis), Arrays.asList(shuttles));
        System.out.println(taxico.taxis.size()); //2
        System.out.println(taxico.shuttles.size()); //3
        */
        /*
        Taxi[] taxis = new Taxi[2];
        Arrays.setAll(taxis, (e) -> {return new Taxi(null);});
        Shuttle[] shuttles = new Shuttle[3];
        Arrays.setAll(shuttles, (e) -> {return new Shuttle(null);});
        TaxiCompany taxico = new TaxiCompany(taxis, shuttles);
        System.out.println(taxico.requestPickup(new Passenger(null, new Location(), new Location()), Taxi.class)); //true
        System.out.println(taxico.requestPickup(new Passenger(null, new Location(), new Location()), Shuttle.class)); //true
        */
        /*
        Taxi[] taxis = new Taxi[2];
        Arrays.setAll(taxis, (e) -> {return new Taxi(null);});
        TaxiCompany taxico = new TaxiCompany(Arrays.asList(taxis), null);
        Location pickupLocation = new Location();
        Passenger passenger = new Passenger(null, pickupLocation, null);
        Taxi taxi = new Taxi(new Location());
        taxico.taxiAssignments.put(taxi, passenger);
        taxico.arrivedAtPickup(taxi);
        System.out.println(taxi.location == passenger.getPickupLocation()); //true
        */
        /*
        Taxi[] taxis = new Taxi[2];
        Arrays.setAll(taxis, (e) -> {return new Taxi(null);});
        TaxiCompany taxico = new TaxiCompany(Arrays.asList(taxis), null);
        Location pickupLocation = new Location();
        Passenger passenger = new Passenger(null, pickupLocation, null);
        Taxi taxi = new Taxi(new Location());
        taxico.taxiAssignments.put(taxi, passenger);
        taxico.arrivedAtPickup(taxi);
        taxico.arrivedAtDestination(taxi, passenger);
        System.out.println(taxi.location == passenger.getDestination()); //true
        System.out.println(taxi.isFree()); //true
        */
        /*
        PassengerSource passengerSource = new PassengerSource(new TaxiCompany(new Taxi[]{}, new Shuttle[]{}));
        System.out.println(passengerSource.requestPickup(Taxi.class)); //false
        System.out.println(passengerSource.requestPickup(Shuttle.class)); //false
        Taxi[] taxis = new Taxi[2];
        Arrays.setAll(taxis, (e) -> {return new Taxi(null);});
        Shuttle[] shuttles = new Shuttle[3];
        Arrays.setAll(shuttles, (e) -> {return new Shuttle(null);});
        passengerSource = new PassengerSource(new TaxiCompany(taxis, shuttles));
        System.out.println(passengerSource.requestPickup(Taxi.class)); //true
        System.out.println(passengerSource.requestPickup(Shuttle.class)); //true
        */
    }

}
