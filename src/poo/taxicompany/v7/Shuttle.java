package poo.taxicompany.v7;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.Optional;


/**
 * A shuttle is able to carry multiple passengers.
 * The shuttle is initially idle and empty of passengers - it sits at a
 * location waiting for a pickup location.
 * When it receives a pickup location, that gets added to the list of
 * destinations and a target location is chosen from the list.
 * Whenever a new target is chosen from the destinations list, it is
 * removed from the list.
 * When the shuttle picks up a passenger they are added to the list of
 * passengers. The passenger and shuttle are then at the same location.
 * The passenger's pickup location is removed from the list of destinations
 * and their destination is added to the list.
 * A new target location is chosen from the list of destinations.
 * When the shuttle offloads a passenger at the target location, they
 * are removed from the list of passengers.
 * A new target location is chosen from the list of destinations.
 * Much depends on the definition of isFree. For the moment we'll use about the
 * simplest implementation - the number of passengers carried is the maximum
 * capacity of the shuttle.
 * For the moment, choosing the next target location is about as simple as
 * possible - a random choice from the destination list.
 * For the moment, again, we ignore how the shuttle actually moves from one
 * location to another.
 *
 * @author David J. Barnes and Michael Kölling
 * @author <code><a href="mailto:sander@unice.fr">Peter Sander</a></code>
 */
public class Shuttle {
    // default max number of possible passengers
    public static final int MAX_CAPACITY = 8;
    // max number of possible passengers
    private int maxCapacity;
    // shuttle's current location
    Location location;  // package access for testing
    // where the shuttle is headed
    Location targetLocation;  // package access for testing
    // list of destinations for the shuttle
    List<Location> destinations = new ArrayList<>();  // package access for testing
    // list of passengers on the shuttle
    List<Passenger> passengers = new ArrayList<>();  // package access for testing

    /**
     * Default constructor for objects of class Shuttle.
     * Assigns the default maxCapacity as maxCapacity.
     * @param location The shuttle's starting point.
     */
    public Shuttle(Location location) {
        this(MAX_CAPACITY, location);
    }

    /**
     * Constructor for objects of class Shuttle
     * @param maxCapacity Max number of passengers the shuttle can carry;
     * @param location The shuttle's starting point.
     */
    public Shuttle(int maxCapacity, Location location) {
        this.maxCapacity = maxCapacity;
        this.location = location;
    }

    /**
     * A shuttle is free when it is carrying less than its max capacity of
     * passengers.
     * @return Whether or not this shuttle is free.
     */
    public boolean isFree() {
        return passengers.size() < maxCapacity;
    }
    
    /**
     * Receive a pickup location.
     * If the shuttle is free, the location is added to the list of
     * destinations and a new target location is chosen from the list.
     * @param location The pickup location.
     * @return true if the pickup location has been added to the destinations,
     * false otherwise.
     */
    public boolean setPickupLocation(Location location) {
        if (isFree()) {
            destinations.add(location);
            chooseTargetLocation();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Determines whether a location is one of the shuttle destinations.
     * @param location Potential destination, or not.
     * @return true if the location is in fact a destination, false if not.
     */
    boolean isaDestination(Location location) {
        return destinations.contains(location);
    }
    
    /**
     * Receive a passenger. This means that that shuttle has somehow
     * moved to the passenger's location.
     * Remove the current location from the destinations (because the shuttle
     * is there already), and add the passenger's destination to the list.
     * A new target location is chosen.
     * @param passenger The passenger.
     * @return true if tha passenger was picked up; false otherwise.
     */
    public boolean pickup(Passenger passenger) {
        if (isFree()) {
            location = passenger.getPickupLocation();
            destinations.remove(passenger.getPickupLocation());
            passengers.add(passenger);
            destinations.add(passenger.getDestination());
            chooseTargetLocation();
            return true;
        }
        return false;
    }

    /**
     * Offload the passenger whose destination is the current target location.
     * This means the taxi has somehow moved to the target location.
     * Removes the passenger from the list of passengers.
     * A new  target location is chosen.
     * @return true if tha passenger was picked up; false otherwise.
     */
    public boolean offload() {
        // determine which passenger corresponds to target location
        Passenger passenger = passengerFromTargetLocation(targetLocation);
        if (passenger != null) {
            location = targetLocation;
            passengers.remove(passenger);
            targetLocation = null;
            chooseTargetLocation();
            return true;
        }
        return false;
    }

    // package access for testing
    Passenger passengerFromTargetLocation(Location target) {
        Optional<Passenger> opt = passengers.stream().filter(
                p -> target == p.getPickupLocation()
                        || target == p.getDestination()).findFirst();
        return opt.isPresent() ? opt.get() : null;
    }

    /**
     * Decides new target location, based on the list of possible destinations.
     * If a target location already exists, then it is not changed.
     * Removes new targetLocation from the list of destinations.
     * Currently we don't really know how to do this correctly so just
     * takes a random destination.
     * @return New target location;
     */
    Location chooseTargetLocation() {  // package access for testing
        if (targetLocation != null) {
            destinations.add(targetLocation);  // back onto destinations
        }
        if (destinations.size() > 0) {
            int index = new Random().nextInt(destinations.size());
            targetLocation = destinations.remove(index);
        }
        return targetLocation;
    }
}
