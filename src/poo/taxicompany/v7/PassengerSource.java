package poo.taxicompany.v7;

/**
 * Simulate passengers requesting rides from a taxi company.
 * Passengers should be generated at random intervals.
 *
 * @author David J. Barnes and Michael Kölling
 * @author [Your name here]
 */
public class PassengerSource {

    // attributes as necessary
    TaxiCompany taxiCo;
    int passengers = 1;

    /**
     * Constructor for objects of class PassengerSource.
     * @param taxiCo The taxi company to be used.
     */
    public PassengerSource(TaxiCompany taxiCo) {
        this.taxiCo = taxiCo;
    }

    /**
     * Have the source generate a new passenger and
     * request a pickup from the company.
     * @param vehicleClass What kind of vehicle to pick up the passenger
     * @return true if the request succeded; false otherwise.
     */
    public boolean requestPickup(Class<?> vehicleClass) {
        return taxiCo.requestPickup(createPassenger(), vehicleClass);
    }

    /**
     * Create a new passenger.
     * The first passenger created is named "Passenger1",
     * the second is "Passenger2" and so on for each new
     * passenger.
     * @return The created passenger.
     */
    Passenger createPassenger() { // package access for testing
        passengers++;
        return new Passenger("Passenger"+passengers,new Location(), new Location());
    }
}