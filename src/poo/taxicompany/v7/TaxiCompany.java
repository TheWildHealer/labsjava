package poo.taxicompany.v7;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

public class TaxiCompany {
    List<Taxi> taxis;  // package access for testing
    List<Shuttle> shuttles;  // package access for testing

    /* When a pickup request is received and a vehicle is scheduled,
    the taxi company maintains an association between the vehicle and the
    passenger the vehicle is to pick up.
    */
    // assignments are package access for testing
    Map<Taxi, Passenger> taxiAssignments = new HashMap<>();
    Map<Shuttle, Passenger> shuttleAssignments = new HashMap<>();

    /**
     * Constructor for objects of class TaxiCompany.
     * Note that this constructor has array arguments, but that
     * taxis and shuttles are stored as lists.
     * @param taxis Company's taxis.
     * @param shuttles Company's shuttles.
     */
    public TaxiCompany(Taxi[] taxis, Shuttle[] shuttles) {
        this.taxis = Arrays.asList(taxis);
        this.shuttles = Arrays.asList(shuttles);
    }

    /**
     * Constructor for objects of class TaxiCompany.
     * @param taxis Company's taxis.
     * @param shuttles Company's shuttles.
     */
    public TaxiCompany(List<Taxi> taxis, List<Shuttle> shuttles) {
        this.taxis = taxis;
        this.shuttles = shuttles;
    }

    // methods

    /**
     * Request a pickup for a given passenger and a given type of vehicle,
     * either a taxi or a shuttle.
     * Chooses an available taxi via the scheduleTaxi method, or an
     * available shuttle via the scheduleShuttle method.
     * Routes the request to the available vehicle by calling its
     * setPickupLocation method.
     * Assigns passenger to their vehicle in either the taxiAssignments
     * or the shuttleAssignments map.
     * You may want to look at the documentation for java.lang.Class.
     * @param passenger The passenger requesting a pickup.
     * @param vehicleClass The class of the pickup vehicle.
     * @return Whether a free vehicle of the requested class is available.
     */
    public boolean requestPickup(Passenger passenger, Class<?> vehicleClass) {
        if(Shuttle.class.equals(vehicleClass)){
            Shuttle tempS;
            tempS = scheduleShuttle();
            if(tempS == null) return false;
            tempS.setPickupLocation(passenger.getPickupLocation());
            shuttleAssignments.put(tempS, passenger);
            return true;
        }else if(Taxi.class.equals(vehicleClass)){
            Taxi tempT;
            tempT = scheduleTaxi();
            if(tempT == null) return false;
            tempT.setPickupLocation(passenger.getPickupLocation());
            taxiAssignments.put(tempT, passenger);
            return true;
        }
        return false;
    }

    /**
     * Find a free taxi, if any.
     * Currently just selects first free taxi.
     * @return A free taxi, or null if there is none.
     */
    private Taxi scheduleTaxi() {
        for(Taxi taxi : this.taxis) {
            if(taxi.isFree()) {
                return taxi;
            }
        }
        return null;
    }

    /**
     * Find a free shuttle, if any.
     * Currently just selects first free shuttle.
     * @return A free shuttle, or null if there is none.
     */
    private Shuttle scheduleShuttle() {
        for(Shuttle shuttle : this.shuttles) {
            if(shuttle.isFree()) {
                return shuttle;
            }
        }
        return null;
    }

    /**
     * A taxi has arrived at a pickup point.
     * Picks up passenger. Remember that you assigned a passenger
     * to a taxi in the taxiAssignments map.
     * @param taxi The taxi at the pickup point.
     * @return true if passenger picked up; false otherwise.
     */
    public boolean arrivedAtPickup(Taxi taxi) {
        return taxi.pickup(taxiAssignments.get(taxi));
    }

    /**
     * A shuttle has arrived at a pickup point.
     * Picks up passenger. Remember that you assigned a passenger
     * to a shuttle in the shuttleAssignments map.
     * @param shuttle The shuttle at the pickup point.
     * @return true if assigned passenger was picked up; false otherwise.
     */
    public boolean arrivedAtPickup(Shuttle shuttle) {
        return shuttle.pickup(shuttleAssignments.get(shuttle));
    }

    /**
     * A taxi has arrived at a passenger's destination.
     * Offloads passenger.
     * @param taxi The taxi at the destination.
     * @param passenger The passenger being dropped off.
     * @return true if passenger offladed; false otherwise.
     */
    public boolean arrivedAtDestination(Taxi taxi, Passenger passenger) {
        if(taxi.offload()) {
            taxiAssignments.remove(taxi, passenger);
            return true;
        }
        return false;
    }

    /**
     * A shuttle has arrived at a passenger's destination.
     * @param shuttle The shuttle at the destination.
     * @param passenger The passenger being dropped off.
     * @return true if passenger offloaded; false otherwise.
     */
    public boolean arrivedAtDestination(Shuttle shuttle, Passenger passenger) {
        if(shuttle.offload()) {
            shuttleAssignments.remove(shuttle, passenger);
            return true;
        }
        return false;
    }

}