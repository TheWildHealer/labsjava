package poo.taxicompany.v7;


/**
 * A taxi is able to carry a single passenger.
 * The taxi is initially idle - it sits at a location waiting for a
 * pickup location.
 * When it receives a pickup location, that becomes its target location
 * to pick up a passenger.
 * The taxi picks up a passenger from the target location.
 * The passenger and taxi are at the same location.
 * The passenger's destination becomes the taxis new target location.
 * When the taxi offloads the passenger at the target location.
 * This becomes its new location, and its target location becomes
 * null until it receives the next pickup request.
 * For the moment, we ignore how the taxi actually moves from one
 * location to another.
 * 
 * @author David J. Barnes and Michael Kölling
 * @author <code><a href="mailto:sander@unice.fr">Peter Sander</a></code>
 */
public class Taxi {
    // where the taxi is
    Location location;  // package access for testing
    // where the taxi is headed.
    Location targetLocation;  // package access for testing
    // taxi passenger
    private Passenger passenger;

    /**
     * Constructor for objects of class Taxi.
     * @param location The taxi's starting point.
     */
    public Taxi(Location location) {
        this.location = location;
    }

    /**
     * A taxi is free when it is not moving to pick up a passenger
     * and is not taking a passenger to their destination.
     * @return Whether or not this taxi is free.
     */
    public boolean isFree() {
        return targetLocation == null;
    }
    
    /**
     * Receive a pickup location.
     * This becomes the target location.
     * @param location The pickup location.
     * @return true if location set; false otherwise.
     */
    public boolean setPickupLocation(Location location) {
        if (isFree()) {
            targetLocation = location;
            return true;
        }
        return false;
    }
    
    /**
     * Receive a passenger. This means the taxi has somehow moved to the
     * passenger's location.
     * The passenger's destination becomes the target location.
     * @param passenger The passenger.
     * @return true if the passenger picked up; false otherwise.
     */
    public boolean pickup(Passenger passenger) {
        this.passenger = passenger;
        location = passenger.getPickupLocation();
        targetLocation = passenger.getDestination();
        return !(this.passenger == null || targetLocation == null);
    }

    /**
     * Offload the passenger. This means the taxi has somehow moved to the
     * target location.
     * The taxi's target location becomes null (until it receives the next
     * pickup request).
     * The taxi is free to pick up next passenger.
     * @return true if passenger offloaded; false otherwise.
     */
    public boolean offload() {
        location = targetLocation;
        passenger = null;
        targetLocation = null;
        return passenger == null && targetLocation == null && isFree();
    }
}
