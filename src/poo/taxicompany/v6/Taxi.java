package poo.taxicompany.v6;


/**
 * A taxi is able to carry a single passenger.
 * The taxi is initially idle - it sits at a location waiting for a
 * pickup location.
 * When it receives a pickup location, that becomes its target location
 * to pick up a passenger.
 * The taxi picks up a passenger from the target location.
 * The passenger and taxi are at the same location.
 * The passenger's destination becomes the taxis new target location.
 * The taxi offloads the passenger at the target location.
 * This becomes its new location, and its target location becomes
 * null until it receives the next pickup request.
 * For the moment, we ignore how the taxi actually moves from one
 * location to another.
 *
 * @author David J. Barnes and Michael Koelling
 * @author [Your name here]
 */
public class Taxi {
    // where the taxi is
    Location location;  // package access for testing
    // where the taxi is headed.
    Location targetLocation;  // package access for testing
    // taxi passenger
    private Passenger passenger;

    /**
     * Constructor for objects of class Taxi.
     * @param location The taxi's starting point. Must not be null.
     */
    public Taxi(Location location) {
        this.location = location;
    }

    /**
     * Get the target location.
     * @return Where this vehicle is currently headed, or null
     * if it is idle.
     */
    public Location getTargetLocation() {
        return targetLocation;
    }

    /**
     * A taxi is free when it is not moving to pick up a passenger
     * and is not taking a passenger to their destination.
     * @return Whether or not this taxi is free.
     */
    public boolean isFree() {
        return (targetLocation == null && passenger == null);
    }

    /**
     * Receive a pickup location.
     * This becomes the target location.
     * @param location The pickup location.
     */
    public boolean setPickupLocation(Location location) {
        if(!isFree()) return false;
        this.targetLocation = location;
        return true;
    }

    /**
     * Receive a passenger. This means the taxi has somehow moved to the
     * passenger's location.
     * The passenger's destination becomes the target location.
     * @param passenger The passenger.
     */
    public boolean pickup(Passenger passenger) {
        this.passenger = passenger;
        this.targetLocation = passenger.getDestination();
        this.location = passenger.getPickupLocation();
        return true;
    }

    /**
     * Offload the passenger. This means the taxi has somehow moved to the
     * passenger's destination.
     * The taxi's target location becomes null (until it receives the next
     * pickup request).
     */
    public boolean offload() {
        passenger = null;
        location = targetLocation;
        targetLocation = null;
        return true;
    }
}
