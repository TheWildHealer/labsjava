package poo.taxicompany.v6;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * A shuttle is able to carry multiple passengers.
 *
 * @author David J. Barnes and Michael Kölling
 * @author Theo Frasquet
 */
public class Shuttle {
    // by default shuttle capacity is 8;
    List<Location> destinations;
    List<Passenger> passengers;
    public final static int MAX_CAPACITY = 8;
    private int capacity;
    Location location;
    Location targetLocation;

    /**
     * Default constructor for objects of class Shuttle
     * @param location The vehicle's starting point. Must not be null.
     */
    public Shuttle(Location location) {
        this(MAX_CAPACITY, location);
    }

    /**
     * Constructor for objects of class Shuttle
     * @param capacity Number of passengers the shuttle can carry;
     * @param location The vehicle's starting point. Must not be null.
     */
    public Shuttle(int capacity, Location location) {
        destinations = new ArrayList<>();
        passengers = new ArrayList<>();
        this.location = location;
        this.capacity = capacity;
    }

    /**
     * Is the shuttle free?
     * @return Whether or not this vehicle is free.
     */
    public boolean isFree() {
        return passengers.size()<capacity;
    }

    /**
     * Decides new target location, based on the list of possible destinations.
     * If a target location already exists, then it is not changed.
     * Removes new targetLocation from the list of destinations.
     * Currently we don't really know how to do this correctly so just
     * chooses a random destination.
     * @return New target location;
     */
    Location chooseTargetLocation() {
        if(targetLocation != null) return targetLocation;
        if(destinations.size() == 0) return null;
        targetLocation = destinations.remove((new Random()).nextInt(destinations.size()));
        return targetLocation;
    }

    /**
     * Get the current location.
     * @return Where this vehicle is currently located.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Get the target location.
     * @return Where this vehicle is currently headed, or null
     * if it is idle.
     */
    public Location getTargetLocation() {
        return destinations.get(0);
    }

    /**
     * Receive a pickup location.
     * If the shuttle is free, the location is added to the list of
     * destinations and a new target location is chosen from the list.
     * @param location The pickup location.
     * @return true if the pickup location has been added to the destinations,
     * false otherwise.
     */
    public boolean setPickupLocation(Location location) {
        if(!isFree()) return false;
        destinations.add(location);
        targetLocation = null;
        chooseTargetLocation();
        return true;
    }

    /**
     * Determines whether a location is one of the shuttle destinations.
     * @param location Potential destination, or not.
     * @return true if the location is in fact a destination, false if not.
     */
    boolean isaDestination(Location location) {
        return destinations.contains(location);
    }

    /**
     * Receive a passenger. This means that that shuttle has somehow
     * moved to the passenger's location.
     * Remove the current location from the destinations (because the shuttle
     * is there already), and add the passenger's destination to the list.
     * A new target location is chosen.
     * @param passenger The passenger.
     * @return true if tha passenger was picked up; false otherwise.
     */
    public boolean pickup(Passenger passenger) {
        if(!isFree()) return false;
        location = passenger.getPickupLocation();
        destinations.remove(location);
        destinations.add(passenger.getDestination());
        passengers.add(passenger);
        chooseTargetLocation();
        return true;
    }

    /**
     * Offload the passenger whose destination is the current target location.
     * This means the shuttle has somehow moved to the target location.
     * Removes the passenger from the list of passengers.
     * A new  target location is chosen.
     * @return true if tha passenger was offloaded; false otherwise.
     */
    public boolean offload() {
        location = targetLocation;
        targetLocation = null;
        Optional<Passenger> opt = passengers.stream().filter(passenger -> passenger.getDestination() == location).findFirst();
        if(!opt.isPresent()) return false;
        passengers.remove(opt.get());
        chooseTargetLocation();
        return true;
    }
}