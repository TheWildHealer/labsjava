package poo.taxicompany.v4;

/**
 * Created by Theo on 09/09/2016.
 */
public class Passenger {
    private Location destination;
    private Location location;

    public Passenger(String name, Location location, Location destination) {
        this.destination = destination;
        this.location = location;
    }

    public Location getDestination() {
        return destination;
    }

    public Location getPickupLocation() {
        return location;
    }

}
