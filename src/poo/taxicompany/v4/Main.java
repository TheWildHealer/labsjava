package poo.taxicompany.v4;

/**
 * Created by Theo on 14/09/2016.
 */
public class Main {

    public static void main(String[] args) {
        Taxi occupied = new Taxi(new Location());
        Taxi free = new Taxi(new Location());
        TaxiCompany taxiCompany = new TaxiCompany(occupied, free);
        taxiCompany.requestPickup(new Passenger("Red", new Location(), new Location()));
        System.out.println(taxiCompany.scheduleTaxi() == free);
    }

}
