package poo.taxicompany.v4;

import java.util.List;
import java.util.ArrayList;

/**
 * Model the operation of a taxi company, operating different
 * types of vehicle.
 *
 * @author David J. Barnes and Michael Kölling* @author [Your name here] */
public class TaxiCompany {
    private List<Taxi> taxis;
    private List<Passenger> passengers;

    /**
     * Constructor for objects of class TaxiCompany.
     * @param taxis Company's taxis.
     */
    public TaxiCompany(List<Taxi> taxis) {
        this.taxis = taxis;
        this.passengers = new ArrayList<>();
    }

    /**
     * Constructor for objects of class TaxiCompany.
     * @param taxis Company's taxis.
     */
    public TaxiCompany(Taxi... taxis) {
        this.taxis = new ArrayList<>();
        this.passengers = new ArrayList<>();
        for(Taxi taxi:taxis) {
            this.taxis.add(taxi);
        }
    }

    /**
     * Request a pickup for the given passenger.
     * @param passenger The passenger requesting a pickup.
     * @return Whether a free taxi is available.
     */
    public boolean requestPickup(Passenger passenger) {
        if(scheduleTaxi() == null) return false;
        scheduleTaxi().setPickupLocation(passenger.getPickupLocation());        passengers.add(passenger);
        return true;
    }

    /**
     * A taxi has arrived at a pickup point.
     * @param taxi The taxi at the pickup point.
     */
    public void arrivedAtPickup(Taxi taxi) {
        for(Passenger pass : passengers){
            if(pass.getPickupLocation().equals(taxi.getTargetLocation())) {
                taxi.pickup(pass);
                return;
            }
        }
    }

    /**
     * A taxi has arrived at a passenger's destination.
     * @param taxi The taxi at the destination.
     * @param passenger The passenger being dropped off.
     */
    public void arrivedAtDestination(Taxi taxi, Passenger passenger) {
        taxi.offload();
        passengers.remove(passenger);
    }

    /**
     * Find a free taxi, if any.
     * @return A free taxi, or null if there is none.
     */
    Taxi scheduleTaxi() {
        for(Taxi taxi : this.taxis) {
            if(taxi.isFree()) {
                return taxi;
            }
        }
        return null;
    }
}