package poo.taxicompany.v2;

import java.util.List;
import java.util.ArrayList;

/**
 * A shuttle is able to carry multiple passengers.
 *
 * @author David J. Barnes and Michael Kölling
 * @author Theo Frasquet
 */
public class Shuttle {
    // by default shuttle capacity is 8;
    private List<Location> destinations;
    private List<Passenger> passengers;
    private int capacity = 8;
    private int toPickup = 0;
    private Location location;

    /**
     * Default constructor for objects of class Shuttle
     * @param location The vehicle's starting point. Must not be null.
     */
    public Shuttle(Location location) {
        if(location != null) {
            destinations = new ArrayList<>();
            passengers = new ArrayList<>();
            this.location = location;
        }
    }

    /**
     * Constructor for objects of class Shuttle
     * @param capacity Number of passengers the shuttle can carry;
     * @param location The vehicle's starting point. Must not be null.
     */
    public Shuttle(int capacity, Location location) {
        if(location != null) {
            destinations = new ArrayList<>();
            passengers = new ArrayList<>();
            this.location = location;
            this.capacity = capacity;
        }
    }

    /**
     * Is the shuttle free?
     * @return Whether or not this vehicle is free.
     */
    public boolean isFree() {
        return passengers.size()+toPickup<capacity;
    }

    /**
     * Get the current location.
     * @return Where this vehicle is currently located.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Get the target location.
     * @return Where this vehicle is currently headed, or null
     * if it is idle.
     */
    public Location getTargetLocation() {
        return destinations.get(0);
    }

    /**
     * Receive a pickup location.
     * @param location The pickup location.
     * @return true if the pickup location has been added to the shuttle
     * destinations, false otherwise, eg, if the shuttle is full.
     */
    public boolean setPickupLocation(Location location) {
        if(!isFree()) return false;
        toPickup++;
        destinations.add(location);
        return true;
    }

    /**
     * Determines whether a location is one of the shuttle destinations.
     * @param location Potential destination, or not.
     * @return true if the location is in fact a destination, false if not.
     */
    boolean isaDestination(Location location) {
        return destinations.contains(location);
    }

    /**
     * Receive a passenger.
     * Add their destination to the list.
     * @param passenger The passenger.
     */
    public void pickup(Passenger passenger) {
        if(toPickup>0) toPickup--;
        destinations.add(passenger.getDestination());
        if(destinations.size()>1) location = destinations.remove(0);
        passengers.add(passenger);
    }

    /**
     * Offload a passenger whose destination is the
     * current location.
     */
    public void offloadPassenger() {
        if(passengers.get(0).getDestination().equals(destinations.get(0))) {
            passengers.remove(0);
            location = destinations.remove(0);
        }
    }
}