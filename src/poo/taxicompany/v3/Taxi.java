package poo.taxicompany.v3;

/**
 * A taxi is able to carry a single passenger.
 *
 * @author David J. Barnes and Michael Kölling
 * @author Theo Frasquet
 */
public class Taxi {
    private Location location;
    private Location targetLocation;
    private boolean free;

    /**
     * Constructor for objects of class Taxi
     * @param location The vehicle's starting point.
     */
    public Taxi(Location location) {
        this.location = location;
        free = true;
    }

    /**
     * Is the taxi free?
     * @return Whether or not this taxi is free.
     */
    public boolean isFree() {
        return free;
    }


    /**
     * Get the current location.
     * @return Where this vehicle is currently located.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Get the target location.
     * @return Where this vehicle is currently headed, or null
     * if it is idle.
     */
    public Location getTargetLocation() {
        return targetLocation;
    }

    /**
     * Receive a pickup location. This becomes the
     * target location.
     * @param location The pickup location.
     */
    public void setPickupLocation(Location location) {
        this.targetLocation = location;
    }

    /**
     * Receive a passenger.
     * Set their destination as the target location.
     * @param passenger The passenger.
     */
    public void pickup(Passenger passenger)  {
        free = false;
        targetLocation = passenger.getDestination();
    }

    /**
     * Offload the passenger.
     */
    public void offload() {
        location = targetLocation;
        targetLocation = null;
        free = true;
    }
}