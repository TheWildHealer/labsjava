package poo.taxicompany.v3;

/**
 * A shuttle is able to carry multiple passengers.
 *
 * @author David J. Barnes and Michael Kölling
 * @author Theo Frasquet
 */
public class Shuttle {
    // by default shuttle capacity is 8;
    private Location[] destinations;
    private Passenger[] passengers;
    private int capacity = 8;
    private int toPickup = 0;
    private int pickedup = 0;
    private int dest;
    private Location location;

    /**
     * Default constructor for objects of class Shuttle
     * @param location The vehicle's starting point. Must not be null.
     */
    public Shuttle(Location location) {
        if(location != null) {
            destinations = new Location[100];
            passengers = new Passenger[capacity];
            this.location = location;
        }
    }

    /**
     * Constructor for objects of class Shuttle
     * @param capacity Number of passengers the shuttle can carry;
     * @param location The vehicle's starting point. Must not be null.
     */
    public Shuttle(int capacity, Location location) {
        if(location != null) {
            this.capacity = capacity;
            destinations = new Location[100];
            passengers = new Passenger[capacity];
            this.location = location;
        }
    }

    /**
     * Is the shuttle free?
     * @return Whether or not this vehicle is free.
     */
    public boolean isFree() {
        return pickedup+toPickup<capacity;
    }

    /**
     * Get the current location.
     * @return Where this vehicle is currently located.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Get the target location.
     * @return Where this vehicle is currently headed, or null
     * if it is idle.
     */
    public Location getTargetLocation() {
        return destinations[0];
    }

    /**
     * Receive a pickup location.
     * @param location The pickup location.
     * @return true if the pickup location has been added to the shuttle
     * destinations, false otherwise, eg, if the shuttle is full.
     */
    public boolean setPickupLocation(Location location) {
        if(!isFree()) return false;
        toPickup++;
        destinations[dest] = location;
        dest++;
        return true;
    }

    /**
     * Determines whether a location is one of the shuttle destinations.
     * @param location Potential destination, or not.
     * @return true if the location is in fact a destination, false if not.
     */
    public boolean isaDestination(Location location) {
        for(int i =0; i<dest; i++) {
            if (destinations[i].equals(location)) return true;
        }
        return false;
    }

    /**
     * Receive a passenger.
     * Add their destination to the list.
     * @param passenger The passenger.
     */
    public void pickup(Passenger passenger) {
        pickedup++;
        toPickup--;
        destinations[dest] = passenger.getDestination();
        dest++;
        if(dest>1) {
            location = destinations[0];
            for (int i = 0; i < destinations.length - 1; i++) {
                destinations[i] = destinations[i + 1];
            }
            destinations[dest - 1] = null;
            dest--;
        }
        passengers[pickedup] = passenger;
    }

    /**
     * Offload a passenger whose destination is the
     * current location.
     */
    public void offloadPassenger() {
        location = destinations[0];
        for(int i = 0; i <destinations.length-1; i++) {
            destinations[i] = destinations[i+1];
        }
        destinations[dest-1] = null;
        for(int i = 0; i <passengers.length-1; i++) {
            passengers[i] = passengers[i+1];
        }
        passengers[pickedup-1] = null;
        pickedup--;
        dest--;
    }
}