package poo.facebouc.v2;


/**
 * This class stores information about a post in a social network. The main part
 * of the post consists of a (possibly multi-line) text message. Other data,
 * such as author and time, are also stored.
 * 
 * @author Michael Kölling and David J. Barnes
 * @author Peter Sander
 */
public class MessagePost extends Post {
    private String message; // an arbitrarily long, multi-line message

    /**
     * Constructor for objects of class MessagePost.
     * 
     * @param author
     *            The username of the author of this post.
     * @param text
     *            The text of this post.
     */
    public MessagePost(String author, String text) {
        super(author);
        message = text;
    }

    /**
     * Return the text of this post.
     * 
     * @return The post's text.
     */
    public String getText() {
        return message;
    }

    /**
     * Display the details of this post as follows:
     *
username
message
timestamp<<likes>>
<<comments>>
     *
     * where
     * <<likes>> is
     * "  -  <<N>> people like this"
     * if there are N likes (the number of likes replaces <<N>>); otherwise
     *  nothing
     * <<comments>> is 
     * "   <<M>> comment(s). Click here to view."
     * if there are M comments; otherwise
     * "   No comments."
     * if there are no comments
     *
     * Eg,
Leonardo da Vinci
Code this...!
0 seconds ago  -  2 people like this.
   No comments.
     * or
Taylor Twit
Party Like It's 1989
(c)1989 Taylor Twit
30 seconds ago
   12345 comment(s). Click here to view.
     * Pay careful attention to the formatting (whitespace).
     *
     * (Currently: Print to the text terminal. This is simulating display in a
     * web browser for now.)
     */
    @Override
    public void display() {
        display(message + "\n");
    }

}
