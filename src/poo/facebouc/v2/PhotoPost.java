package poo.facebouc.v2;


/**
 * This class stores information about a post in a social network. The main part
 * of the post consists of a photo and a caption. Other data, such as author and
 * time, are also stored.
 * 
 * @author Michael Kölling and David J. Barnes
 * @author Peter Sander
 */
public class PhotoPost extends Post {
    private String filename; // the name of the image file
    private String caption; // a one line image caption

    /**
     * Constructor for objects of class PhotoPost.
     * 
     * @param author
     *            The username of the author of this post.
     * @param filename
     *            The filename of the image in this post.
     * @param caption
     *            A caption for the image.
     */
    public PhotoPost(String author, String filename, String caption) {
        super(author);
        this.filename = filename;
        this.caption = caption;
    }

    /**
     * Return the file name of the image in this post.
     * 
     * @return The post's image file name.
     */
    public String getImageFile() {
        return filename;
    }

    /**
     * Return the caption of the image of this post.
     * 
     * @return The image's caption.
     */
    public String getCaption() {
        return caption;
    }

    /**
     * Display the details of this post as follows:
     *
username
  [<<filename>>]
  <<caption>>
timestamp<<likes>>
<<comments>>
     *
     * where
     * <<likes>> is
     * "  -  <<N>> people like this"
     * if there are N likes (the number of likes replaces <<N>>); otherwise
     *  nothing
     * <<comments>> is 
     * "   <<M>> comment(s). Click here to view."
     * if there are M comments; otherwise
     * "   No comments."
     * if there are no comments
     *
     * Eg,
Alexander Graham Bell
  [handset.png]
  Coming soon: the Samsung Galaxy S7 recall.
0 seconds ago  -  4 people like this.
   No comments.

     * Pay careful attention to the formatting (whitespace).
     * 
     * (Currently: Print to the text terminal. This is simulating display in a
     * web browser for now.)
     */
    @Override
    public void display() {
        display("  [" + filename + "]\n  " + caption + "\n");
    }

}
