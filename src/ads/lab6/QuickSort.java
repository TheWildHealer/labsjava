package ads.lab6;

/**
 * A class for the quicksort algorithm
 */
public class QuickSort {

    private static final int CUTOFF = 10;

    /**
     * Sort the array in place using the quicksort algorithm
     */
    public static <AnyType extends Comparable<AnyType>> void sort(AnyType[] array) {
        sort(array, 0, array.length-1);
    }

    /**
     * Sort the portion array[lo,hi] in place using the quicksort algorithm
     */
    private static <AnyType extends Comparable<AnyType>> void sort(AnyType[] array, int lo, int hi) {
        if(hi - lo >= CUTOFF) {
            int p = partition(array, lo, hi);
            sort(array, lo, p - 1);
            sort(array, p + 1, hi);
        }else
            insertion(array, lo, hi);
    }

    /**
     * Partition the portion array[lo,hi] and return the index of the pivot
     */
    private static <AnyType extends Comparable<AnyType>> int partition(AnyType[] array, int lo, int hi) {
        swap(array, lo, median(array, lo, (lo + hi)/2, hi));
        int a = lo+1;
        for (int i = lo+1; i <= hi; i++) {
            if(array[i].compareTo(array[lo]) < 0)
                swap(array, i, a++);
        }
        swap(array, lo, a-1);
        return a-1;
    }

    /**
     * Return the index of the median of { array[lo], array[mid], array[hi] }
     */
    private static <AnyType extends Comparable<AnyType>> int median(AnyType[] array, int lo, int mid, int hi) {
        if(array[lo].compareTo(array[mid]) < 0 && array[mid].compareTo(array[hi]) < 0 || array[hi].compareTo(array[mid]) < 0 && array[mid].compareTo(array[lo]) < 0)
            return mid;
        if(array[mid].compareTo(array[lo]) < 0 && array[lo].compareTo(array[hi]) < 0 || array[hi].compareTo(array[lo]) < 0 && array[lo].compareTo(array[mid]) < 0)
            return lo;
        return hi;
    }

    /**
     * Sort array[lo, hi] in place using the insertion sort algorithm
     */
    private static <AnyType extends Comparable<AnyType>> void insertion(AnyType[] array, int lo, int hi) {
        for (int i = lo + 1; i <= hi; i++) {
            for (int j = i; j > lo ; j--) {
                if(array[j].compareTo(array[j-1]) < 0)
                    swap(array, j-1, j);
                else
                    break;
            }
        }
    }

    /**
     * Swap array[i] and array[j]
     */
    private static <AnyType> void swap(AnyType[] array, int i, int j) {
        AnyType tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
}
