package ads.lab6;

/**
 * A class for the recursive merge sort algorithm.
 */
public class MergeSort {

    /**
     * Sort the array using the recursive merge sort algorithm.
     * This algorithm is not in place and needs an auxiliary array of
     * the same size than the array to sort
     * Complexity: THETA( n.log(n) ) where n is the size of the array
     */
    @SuppressWarnings("Unchecked")
    public static <AnyType extends Comparable<AnyType>> void sort(AnyType[] array) {
        AnyType[] tmp = (AnyType[]) new Comparable[array.length];
        sort(array,tmp,0,array.length - 1);
    }

    /**
     * Sort the array in the range [lo, hi] using the portion [lo, hi]
     * of the auxiliary array tmp
     * Complexity: THETA( n.log(n) ) where n = hi - lo + 1
     */
    private static <AnyType extends Comparable<AnyType>> void sort(AnyType[] array, AnyType[] tmp, int lo, int hi) {
        if(hi != lo) {
            int mid = (hi + lo)/2;

            sort(array, tmp, lo, mid);
            sort(array, tmp, mid + 1, hi);

            merge(array, tmp, lo, mid, hi);
        }
    }

    /**
     * Merge array[lo, mid] and array[mid+1, hi] into tmp[lo, hi]
     * and copy back the result into array[lo, hi]
     * Precondition: array[lo, mid] and array[mid+1, hi] are sorted
     * Complexity: THETA( n ) where n = hi - lo + 1
     */
    private static <AnyType extends Comparable<AnyType>> void merge(AnyType[] array, AnyType[] tmp, int lo, int mid, int hi) {
        int a = lo, b = mid + 1;

        for (int i = lo; i <= hi; i++)
            if(a > mid)
                tmp[i] = array[b++];
            else if(b > hi || array[a].compareTo(array[b]) < 0)
                tmp[i] = array[a++];
            else
                tmp[i] = array[b++];

        transfer(tmp, array, lo, hi);
    }

    /**
     * Copy the elements from tmp[lo, hi] into array[lo, hi]
     * Complexity: THETA( n ) where n = hi - lo + 1
     */
    private static <AnyType> void transfer(AnyType[] tmp, AnyType[] array, int lo, int hi) {
        System.arraycopy(tmp, lo, array, lo, hi + 1 - lo);
    }
}
