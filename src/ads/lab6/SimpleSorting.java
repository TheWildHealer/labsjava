package ads.lab6;

/**
 * A class for simple sorting methods
 */
public class SimpleSorting {

    /**
     * Sort the array in place using the selection sort algorithm
     */
    public static <AnyType extends Comparable<AnyType>> void selection(AnyType[] array) {
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            AnyType elem = array[i];
            for (int j = i + 1; j < array.length; j++) {
                if(array[j].compareTo(elem) < 0) {
                    elem = array[j];
                    minIndex = j;
                }
            }
            swap(array, i, minIndex);
        }
    }

    /**
     * Sort the array in place using the insertion sort algorithm
     */
    public static <AnyType extends Comparable<AnyType>> void insertion(AnyType[] array) {
        for (int i = 1; i < array.length; i++) {
            for (int j = i; j > 0 ; j--) {
                if(array[j].compareTo(array[j-1]) < 0)
                    swap(array, j-1, j);
                else
                    break;
            }
        }
    }

    /**
     * Swap array[i] and array[j]
     */
    private static <AnyType> void swap(AnyType[] array, int i, int j) {
        AnyType tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
}
