package ads.lab5;

import java.lang.reflect.Array;
import java.util.Comparator;

/**
 * A class for binary heap implementation
 */
public class Dheap<AnyType extends Comparable<? super AnyType>> {

	private AnyType[] heapArray; // to store the heap
	private int size = 0;    // the number of elements in the heap
	private int children = 2;

	// comparator to choose
	private Comparator<AnyType> comparator = new Comparator<AnyType>() {
		public int compare(AnyType e1, AnyType e2) {
			return e1.compareTo(e2);
		}
	};

	///////////// Constructors

	/**
	 * Build a heap of capacity n.
	 * The elements are ordered according to the
	 * natural order on AnyType.
	 * The heap is empty.
	 * Complexity: THETA(1)
	 */
	public Dheap(int n, int children) {
		this(n, children, null);
	}

	/**
	 * Build a heap of capacity n.
	 * The elements are ordered according to comparator.
	 * The heap is empty.
	 * Complexity: THETA(1)
	 */
    @SuppressWarnings("Unchecked")
	public Dheap(int n, int children, Comparator<AnyType> comparator) {
		this.heapArray = (AnyType[]) Array.newInstance(Comparable.class, n);
		this.children = children;
		if(comparator != null)
			this.comparator = comparator;
	}

	/**
	 * Build a heap based on array A.
	 * The elements are ordered according to the
	 * natural order on AnyType.
	 * The heap is full
	 */
	public Dheap(AnyType[] heapArray) {
		this(heapArray, null);
	}

	/**
	 * Build a heap based on array heapArray.
	 * The elements are ordered according to comparator.
	 * The heap is full
	 */
	public Dheap(AnyType[] heapArray, Comparator<AnyType> comparator) {
		this.heapArray =heapArray;
		size = heapArray.length;
		if(comparator != null)
			this.comparator = comparator;
		buildHeap();
	}

	///////////// Private methods

	/**
	 * Swap values in the array
	 * at indexes i and j.
	 * Complexity: THETA(1)
	 */
	private void swap(int i, int j) {
		AnyType tmp = heapArray[i];
		heapArray[i] = heapArray[j];
		heapArray[j] = tmp;
	}

	/**
	 * Return the number of the left
	 * node of node number n.
	 * Complexity: THETA(children)
	 */
	private int[] chlidren(int n) {
		int[] tab = new int[children];
        for (int i = 0; i < children; i++) {
            tab[i] = children*n + i + 1;
        }
        return tab;
	}

	/**
	 * Return the number of the parent
	 * node of node number n.
	 * Complexity: THETA(1)
	 */
	private int parent(int n) {
		return (n - 1)/children;
	}

	// THETA(size)
	private int getMinIndex(int[] tab) {
	    int min = tab[0];
        for (int i = 1; i < children; i++) {
            if(comparator.compare(heapArray[min], heapArray[tab[i]]) > 0 )
                min = tab[i];
        }
        return min;
    }

	/**
	 * Percolate down the element à node number n
	 * Complexity: O(log(size))
	 */
	private void percolateDown(int n) {
	    int min = getMinIndex(chlidren(n));
	    if(comparator.compare(heapArray[min], heapArray[n]) < 0) {
	        swap(n, min);
	        percolateDown(min);
        }
	}

	/**
	 * Percolate up the element à node number n
	 * Complexity: O(log(size))
	 */
	private void percolateUp(int n) {
		if(comparator.compare(heapArray[n], heapArray[parent(n)]) > 0 || n == 0)
			return;
		swap(n, parent(n));
		percolateUp(parent(n));
	}

	/**
	 * Arrange the elements in A such
	 * that it has the heap property.
	 * Complexity: O(size)
	 */
	private void buildHeap() {
		for(int i = size/2; i>=0; i--)
			percolateDown(i);
	}

	///////////// Public methods

	/**
	 * Return the size of the heap
	 * (the number of elements in the heap).
	 * Complexity: THETA(1)
	 */
	public int size() {
		return size;
	}

	/**
	 * Check if the heap is empty.
	 * Complexity: THETA(1)
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Return the extreme element.
	 * Complexity: THETA(1)
	 */
	public AnyType extreme() throws EmptyHeapException {
		if(isEmpty())
			throw new EmptyHeapException();
		return heapArray[0];
	}

	/**
	 * Return and delete the extreme element.
	 * Complexity: O(log(size))
	 */
	public AnyType deleteExtreme() throws EmptyHeapException {
		AnyType temp = extreme();
		heapArray[0] = heapArray[--size];
		percolateDown(0);
		return temp;
	}

	/**
	 * Add a new element in the heap
	 * Complexity: O(log(size))
	 */
	public void add(AnyType e) throws FullHeapException {
		try {
			heapArray[size] = e;
			percolateUp(size++);
		}catch(ArrayIndexOutOfBoundsException ex) {
			throw new FullHeapException();
		}
	}

	///////////// Part 3: deleting in the heap

	/**
	 * Delete the element e from the heap.
	 * Complexity: O(size)
	 */
	public void delete(AnyType e) {
		delete(e, 0);
	}

	private void delete(AnyType e, int i) {
		if(i >= size || comparator.compare(e, heapArray[i]) < 0)
			return;
		if(comparator.compare(e, heapArray[i]) == 0) {
			heapArray[i] = heapArray[--size];
			percolateDown(i);
		}else {
		    int[] tab = chlidren(i);
		    for(int k : tab) {
		        if(comparator.compare(e, heapArray[k]) > 0)
		            delete(e, k);
            }
		}
	}

	/**
	 * Delete all the elements e from the heap.
	 * Complexity: O(size)
	 */
	public void deleteAll(AnyType e) {
		deleteAll(e, 0);
	}

	private void deleteAll(AnyType e, int i) {
		if(i >= size || comparator.compare(e, heapArray[i]) < 0)
			return;
		if(comparator.compare(e, heapArray[i]) == 0) {
			heapArray[i] = heapArray[--size];
			percolateDown(i);
			deleteAll(e, i);
		}else {
            int[] tab = chlidren(i);
            for(int k : tab) {
                if(comparator.compare(e, heapArray[k]) > 0)
                    delete(e, k);
            }
		}
	}
}
