package ads.lab5;

/**
 * A class exception for empty heap
 */
public class EmptyHeapException extends RuntimeException {
	
	public EmptyHeapException() {
		super();
	}

}
