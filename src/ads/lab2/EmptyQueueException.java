package ads.lab2;

/**
 * An exception class for empty queue
 */
public class EmptyQueueException extends RuntimeException {

	public EmptyQueueException() {
		super();
	}
}
