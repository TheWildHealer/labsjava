package ads.lab2;

import java.io.*;
import java.util.*;

/**
 * A class to find pairs (x,y) of integers inside a file
 * matching y = x + n for a given n.
 */
public class Pairing {
	
	/**
	 * Display all the pairs (x,y), x and y  in the file, such that y = x + n
	 * The file 'fileName' contains an entirely increasing (strict) sequence of integers
	 */
	public static void showPairs(int n, String fileName) throws FileNotFoundException, EmptyQueueException {
		Scanner input = new Scanner(new File(fileName));
		int i;
		ListQueue<Integer> q = new ListQueue<Integer>();
		q.enqueue(input.nextInt());
		do {
			i = input.nextInt();
			q.enqueue(i);
			while(q.peek() + n < i)
				q.dequeue();
			if(q.peek() + n == i)
				System.out.println("("+q.peek()+","+i+")");
		}while(input.hasNext());
	}
	
    /**
     * A short main for quick testing
     */
	public static void main(String[] args) throws FileNotFoundException, EmptyQueueException {
		// put the right path here
		showPairs(1273,"big-file.txt");
	}
}
