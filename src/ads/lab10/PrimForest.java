package ads.lab10;

import java.util.*;
import ads.graph.*;
import ads.lab5.*;

/**
 * A class for the Prim algorithm for unconnected graph
 */
@SuppressWarnings("Duplicates")
public class PrimForest {

    /**
     * returns the set all edges of an MST of the graph G
     * the graph G may net be connected
     */
    public static Set<Edge> mst(WeightedUnDiGraph G) throws FullHeapException, EmptyHeapException {

        Set<Edge> mst = new HashSet<>(); // the edges of the MST

        // to make a minimum-heap of weighted edges
        Comparator<WeightedEdge> c = Comparator.reverseOrder();

        // the minimum-heap of weighted edges
        BinaryHeap<WeightedEdge> minHeap = new BinaryHeap<>(G.nbEdges(), c);

        // known[u] == true <==> u is known
        boolean known[] = new boolean[G.nbVertices()];

        setUp(G, minHeap, known, 0);

        WeightedEdge edge;

        while(isntOver(known)) {
            if(minHeap.isEmpty()) {
                setUp(G,minHeap,known,getUnexplored(known));
                continue;
            }
            edge = minHeap.deleteExtreme();
            if(known[edge.destination()])
                continue;
            known[edge.destination()] = true;
            mst.add(edge);
            for(int i : G.adjacents(edge.destination()))
                if(!known[i])
                    minHeap.add(new WeightedEdge(edge.destination(), i, G.weight(edge.destination(), i)));
        }

        return mst;
    }

    private static void setUp(WeightedUnDiGraph G, BinaryHeap<WeightedEdge> minHeap, boolean[] known, int s) throws FullHeapException {
        known[s] = true;

        for(int i : G.adjacents(s))
            minHeap.add(new WeightedEdge(s, i, G.weight(s, i)));
    }

    private static boolean isntOver(boolean known[]) {
        for(boolean b : known)
            if(!b)
                return true;
        return false;
    }

    private static int getUnexplored(boolean known[]) {
        for (int i = 0; i < known.length; i++) {
            if (!known[i])
                return i;
        }
        throw new RuntimeException("C'est pas sensé arriver ça");
    }

    /**
     * for testing
     */
    public static void main(String args[]) throws FullHeapException, EmptyHeapException {
        WeightedUnDiGraph G = new WeightedUnDiGraph(9);
        G.addEdge(0,2,4);
        G.addEdge(0,5,8);
        G.addEdge(2,5,11);

        G.addEdge(1,4,2);
        G.addEdge(1,7,6);
        G.addEdge(3,1,1);
        G.addEdge(3,4,4);
        G.addEdge(3,7,2);

        G.addEdge(6,8,9);

        Set<Edge> mst = mst(G);

        for ( Edge e : mst )
            System.out.print(e + " ");
        System.out.println();
    }
    // expected output (the edges could show up in a different order)
    //
    // (0, 5) (3, 7) (1, 3) (1, 4) (6, 8) (0, 2)
}
