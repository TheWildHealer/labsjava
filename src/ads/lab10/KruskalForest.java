package ads.lab10;

import java.util.*;
import ads.graph.*;
import ads.lab5.*;
import ads.lab7.DisjointSets;

/**
 * A class for the Kruskal algorithm for unconnected graph
 */
@SuppressWarnings("Duplicates")
public class KruskalForest {

    /**
     * adds all the weighted edges of graph G to the minimum heap minHeap
     */
    private static void fillHeap(BinaryHeap<WeightedEdge> minHeap, WeightedUnDiGraph G) throws FullHeapException {
        for ( int u = 0; u < G.nbVertices(); u++ ) {
            for ( Integer a : G.adjacents(u) ) {
                if ( u < a )
                    minHeap.add(new WeightedEdge(u,a,G.weight(u, a)));
            }
        }
    }

    /**
     * returns the set all edges of an MST of the graph G
     * the graph G may net be connected
     */
    public static Set<Edge> mst(WeightedUnDiGraph G) throws FullHeapException, EmptyHeapException {

        Set<Edge> mst = new HashSet<>(); // the edges of the MST

        // to make a minimum-heap of weighted edges
        Comparator<WeightedEdge> c = Comparator.reverseOrder();

        // a minimum-heap
        BinaryHeap<WeightedEdge> minHeap = new BinaryHeap<>(G.nbEdges(), c);
        // fill the minimum-heap with all the weighted edges from the graph G
        fillHeap(minHeap,G);
        // disjoint sets of all the vertices of the graph G
        DisjointSets ds = new DisjointSets(G.nbVertices());

        WeightedEdge edge;

        while(!minHeap.isEmpty()) {
            edge = minHeap.deleteExtreme();
            int r1 = ds.find(edge.origin());
            int r2 = ds.find(edge.destination());
            if(r1 == r2)
                continue;
            ds.union(r1, r2);
            mst.add(edge);
        }

        return mst;
    }

    /**
     * for testing
     */
    public static void main(String args[]) throws FullHeapException, EmptyHeapException {
        WeightedUnDiGraph G = new WeightedUnDiGraph(9);
        G.addEdge(0,2,4);
        G.addEdge(0,5,8);
        G.addEdge(2,5,11);

        G.addEdge(1,4,2);
        G.addEdge(1,7,6);
        G.addEdge(3,1,1);
        G.addEdge(3,4,4);
        G.addEdge(3,7,2);

        G.addEdge(6,8,9);

        Set<Edge> mst = mst(G);

        for ( Edge e : mst )
            System.out.print(e + " ");
        System.out.println();
    }
    // expected output (the edges could show up in a different order)
    //
    // (0, 5) (3, 7) (1, 3) (1, 4) (6, 8) (0, 2)
}
