package tdim;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class ConvolutionFilter extends ImageTransform {
	private static double unTiers = 1.0/3.0;
	protected static final int NAIVE_X=0;
	protected static final int NAIVE_Y=1;
	protected static final int NAIVE=2;
	protected static final int ROBERTS_X=3;
	protected static final int ROBERTS_Y=4;
	protected static final int ROBERTS=5;
	protected static final int SOBEL_X=6;
	protected static final int SOBEL_Y=7;
	protected static final int SOBEL=8;
	protected static final int PREWITT=9;
	protected static final int KIRSCH=10;
	protected static final int LAPLACE4=11;
	protected static final int LAPLACE8=12;
	protected static final int GAUSSIAN3=13;
	protected static final int GAUSSIAN5=14;
	protected static final int GAUSSIAN7=15;
	protected static final int SHARPEN3a=16;
	protected static final int SHARPEN3b=17;
	protected static final int SHARPEN3c=18;
	protected static final int LAPLACE4_ZEROS=19;
	protected static final int LAPLACE8_ZEROS=20;

	protected static final double [][] GAUSSIAN3_FILTER = {{1.0/16,2.0/16,1.0/16},{2.0/16,4.0/16,2.0/16},{1.0/16,2.0/16,1.0/16}};
	protected static final double [][] GAUSSIAN5_FILTER = {{1.0/52,1.0/52,2.0/52,1.0/52,1.0/52},{1.0/52,2.0/52,4.0/52,2.0/52,1.0/52},{2.0/52,4.0/52,8.0/52,4.0/52,2.0/52},{1.0/52,2.0/52,4.0/52,2.0/52,1.0/52},{1.0/52,1.0/52,2.0/52,1.0/52,1.0/52}};
	protected static final double [][] GAUSSIAN7_FILTER = {{1.0/140,1.0/140,2.0/140,2.0/140,2.0/140,1.0/140,1.0/140},{1.0/140,2.0/140,2.0/140,4.0/140,2.0/140,2.0/140,1.0/140},{2.0/140,2.0/140,4.0/140,8.0/140,4.0/140,2.0/140,2.0/140},{2.0/140,4.0/140,8.0/140,16.0/140,8.0/140,4.0/140,2.0/140},{2.0/140,2.0/140,4.0/140,8.0/140,4.0/140,2.0/140,2.0/140},{1.0/140,2.0/140,2.0/140,4.0/140,2.0/140,2.0/140,1.0/140},{1.0/140,1.0/140,2.0/140,2.0/140,2.0/140,1.0/140,1.0/140}};
	protected static final double [][] SHARPEN3a_FILTER = {{0,-1/6.0,0},{-1/6.0,10/6.0,-1/6.0},{0,-1/6.0,0 }};
	protected static final double [][] SHARPEN3b_FILTER = {{1,-2,1},{-2,5,-2},{1,-2,1}};
	protected static final double [][] SHARPEN3c_FILTER = {{-1,-1,-1},{-1,9,-1},{-1,-1,-1}};

	public static final String [] NAMES = {"Naive X", "Naive Y", "Naive", "Roberts X", "Roberts Y","Roberts", "Sobel X", "Sobel Y", "Sobel", "Prewitt", "Kirsch", "Laplace connexity 4", "Laplace connexity 8","Gaussian 3x3","Gaussian 5x5","Gaussian 7x7", "Sharpen type 1", "Sharpen type 2", "Sharpen type 3","Zeros of Laplace 4","Zeros of Laplace 8"};

	protected ArrayList<double[][]> filters; // list of convolution kernel

	protected boolean isLaplacian = false;

	public ConvolutionFilter(double [][] f, String n) {
		super();
		filters = new ArrayList<double[][]>();
		filters.add(f);
		name = n;
	}

	public ConvolutionFilter(double [][] f) {
		super();
		filters = new ArrayList<double[][]>();
		filters.add(f);
		name = "filter :";
		for(int i = 0; i < f.length; i++) {
			name += "[ ";
			for(int j = 0; j < f[0].length; j++)
				name += " " + f[i][j];
			name += " ]";
		}
		name += "\n";
	}

	public ConvolutionFilter(int type) {
		super();
		name = NAMES[type] + " filter";
		filters = new ArrayList<double[][]>();
		double [][] filter;
		switch(type) {
			case NAIVE_X:
				filter = new double[][]{{0, 0, 0}, {-1, 1, 0}, {0, 0, 0}};
                filters.add(filter);
				break;
			case NAIVE_Y:
				break;
			case ROBERTS_X:
				break;
			case ROBERTS_Y:
				break;
			case SOBEL_X:
				break;
			case SOBEL_Y:
				break;
			case LAPLACE4_ZEROS:
			case LAPLACE4:
                filter = new double[][]{{0, -1.0/4.0, 0}, {-1.0/4.0, 1.0, -1.0/4.0}, {0, -1.0/4.0, 0}};
                filters.add(filter);
				break;
			case LAPLACE8_ZEROS:
			case LAPLACE8:
                filter = new double[][]{{-1.0/8.0, -1.0/8.0, -1.0/8.0}, {-1.0/8.0, 1, -1.0/8.0}, {-1.0/8.0, -1.0/8.0, -1.0/8.0}};
                filters.add(filter);
				break;
			case NAIVE:
				break;
			case ROBERTS:
				break;
			case SOBEL:
				break;
			case PREWITT:
				break;
			case KIRSCH:
				break;
			case GAUSSIAN3: //1,2,1,2,4,2,1,2,1 /16
                filters.add(GAUSSIAN3_FILTER);
				break;
			case GAUSSIAN5: //1,1,2,1,1, 1,2,4,2,1, 2,4,8,4,2, 1,2,4,2,1, 1,1,2,1,1 /52
                filters.add(GAUSSIAN5_FILTER);
				break;
			case GAUSSIAN7: //1,1,2,2,2,1,1, 1,2,2,4,2,2,1, 2,2,4,8,4,2,2, 2,4,8,16,8,4,2, 2,2,4,8,4,2,2, 1,2,2,4,2,2,1, 1,1,2,2,2,1,1 / 140
                filters.add(GAUSSIAN7_FILTER);
				break;
			case SHARPEN3a://0,-1,0,-1,10,-1,0,-1,0
                filters.add(SHARPEN3a_FILTER);
				break;
			case SHARPEN3b://1,-2,1,-2,5,-2,1,-2,1
                filters.add(SHARPEN3b_FILTER);
				break;
			case SHARPEN3c://-1,-1,-1,-1,9,-1,-1,-1,-1
                filters.add(SHARPEN3c_FILTER);
				break;

			default:
		}
	}

	public BufferedImage filter(BufferedImage bin) {
		int w = bin.getWidth();
		int h = bin.getHeight();
		BufferedImage bout = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);

		int nbFilters = filters.size();

		switch(nbFilters) {
			case 0:
				return bout;
			case 1:
			    double[][] filter = filters.get(0);
                for( int i = 0; i < w; i++ )
                    for( int j = 0 ; j < h; j++ ) {
                        if(i == 0 || j == 0 || i == w-1 || j == h-1)
                            bout.setRGB(i, j, 0);
                        else {
                            Color c00 = new Color(bin.getRGB(i-1, j-1));
                            Color c01 = new Color(bin.getRGB(i, j-1));
                            Color c02 = new Color(bin.getRGB(i+1, j-1));
                            Color c10 = new Color(bin.getRGB(i-1, j));
                            Color c11 = new Color(bin.getRGB(i, j));
                            Color c12 = new Color(bin.getRGB(i+1, j));
                            Color c20 = new Color(bin.getRGB(i-1, j+1));
                            Color c21 = new Color(bin.getRGB(i, j+1));
                            Color c22 = new Color(bin.getRGB(i+1, j+1));
                            int r = Math.abs((int)(filter[0][0]*c00.getRed() + filter[0][1]*c01.getRed() + filter[0][2]*c02.getRed() + filter[1][0]*c10.getRed() + filter[1][1]*c11.getRed() + filter[1][2]*c12.getRed() + filter[2][0]*c20.getRed() + filter[2][1]*c21.getRed() + filter[2][2]*c22.getRed()));
                            int g = Math.abs((int)(filter[0][0]*c00.getGreen() + filter[0][1]*c01.getGreen() + filter[0][2]*c02.getGreen() + filter[1][0]*c10.getGreen() + filter[1][1]*c11.getGreen() + filter[1][2]*c12.getGreen() + filter[2][0]*c20.getGreen() + filter[2][1]*c21.getGreen() + filter[2][2]*c22.getGreen()));
                            int b = Math.abs((int)(filter[0][0]*c00.getBlue() + filter[0][1]*c01.getBlue() + filter[0][2]*c02.getBlue() + filter[1][0]*c10.getBlue() + filter[1][1]*c11.getBlue() + filter[1][2]*c12.getBlue() + filter[2][0]*c20.getBlue() + filter[2][1]*c21.getBlue() + filter[2][2]*c22.getBlue()));

                            bout.setRGB(i, j, new Color(r, g, b).getRGB());
                        }
                    }
				return bout;
			case 2:
				// TO BE DONE
				// retourne l'image bout resultat de la norme des vecteurs composes
				// de la convolution de l'image bin par chacun des 2 filtres. On utilisera
				// pour norme la norme euclidienne.
				return bout;
			default:
				// TO BE DONE
				// dans le cas de + de 2 filtres, on retourne la norme des vecteurs composes
				// des convolutions de l'image bin par chaque filtre. On utilisera pour cela
				// la norme prenant la plus grande des valeurs absolues.

				return bout;
		}
	}
}
