package tdim;

import java.awt.image.BufferedImage;

public class RestaurationFilters extends ImageTransform {
	protected static final int CONSERVATIVE = 0;
	protected static final int MEDIAN = 1;

	protected static final int CROSS = 0;
	protected static final int DIAMAND = 1;
	protected static final int SQUARE = 2;

	protected static final String [] NAMES = {"Conservative","Median"};
	protected static final String [] NAMES_NHB = {"Cross", "Diamand", "Square"};

	protected int type; // filter type
	protected int nhb;  // neighborough type
	protected int size; // filter size (sizexsize) where size=2p+1
	protected int [][] nmask;
	protected int nbUn; // number of neighbours

	public RestaurationFilters(int t, int n, int s) {
		super();
		type = t;
		nhb = n;
		size = s;
		name = NAMES[type] + " filter with " + NAMES_NHB[nhb] + " neighborough of size " + size;
		nbUn = 0;
		// construction of the neighborouh mask
		nmask = new int[size][size];
		switch(nhb) {
			case CROSS:
				for(int i = 0; i < size; i ++) {
					nmask[i][size/2] = 1;
					nmask[size/2][i] = 1;
					nbUn += 2;
				}
				break;
			case DIAMAND:
				for(int i = 0; i <= size/2; i ++)
					for(int j = size/2-i ; j <= size/2+i; j++) {
						nmask[i][j] = 1;
						nmask[size-i-1][j] = 1;
						if(i==size-i-1) nbUn++;
						else nbUn += 2;
					}
				break;

			case SQUARE:
				for(int i = 0; i < size; i ++)
					for(int j = 0 ; j < size; j++) {
						nmask[i][j] = 1;
						nbUn ++;
					}
				break;
		}
	}

	public BufferedImage filter(BufferedImage bin) {

		int w = bin.getWidth();
		int h = bin.getHeight();
		BufferedImage bout = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);


		if(type == CONSERVATIVE) {
			// TO BE DONE
		}
		else { // MEDIAN
			// TO BE DONE
		}
		return bout;

	}

}
