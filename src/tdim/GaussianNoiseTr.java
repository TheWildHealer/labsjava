package tdim;

import java.awt.image.BufferedImage;
import java.awt.Color;
import java.util.Random;

public class GaussianNoiseTr extends ImageTransform {
	protected double sigma;

	public GaussianNoiseTr(double s) {
		super();
		sigma = s;
		name = "Additive gaussian noise with sigma = " + sigma;
	}

	public BufferedImage filter(BufferedImage bin) {
		int w = bin.getWidth();
		int h = bin.getHeight();

		Random aleatoire = new Random(w);
		BufferedImage bout = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);

		int col, bruit, r, g, b;
		Color c;

		for(int i = 0 ; i < w ; i++ )
			for(int j = 0 ; j < h ; j++ ) {
				c = new Color(bin.getRGB(i,j));
				r = c.getRed();
				g = c.getBlue();
				b = c.getGreen();
				bruit = (int) (sigma * aleatoire.nextGaussian());
				r = r + bruit > 255 ? 255 : r + bruit < 0 ? 0 : r + bruit;
                g = g + bruit > 255 ? 255 : g + bruit < 0 ? 0 : g + bruit;
                b = b + bruit > 255 ? 255 : b + bruit < 0 ? 0 : b + bruit;
                col = new Color(r, g, b).getRGB();
				bout.setRGB(i,j,col);
			}
		return bout;
	}

}
